<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function cw2_blogs_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-control-panel:node/add/blog-post
  $menu_links['menu-control-panel:node/add/blog-post'] = array(
    'menu_name' => 'menu-control-panel',
    'link_path' => 'node/add/blog-post',
    'router_path' => 'node/add/blog-post',
    'link_title' => 'Add a new Blog post',
    'options' => array(
      'attributes' => array(
        'title' => 'Add a new blog post',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'control_panel',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add a new Blog post');


  return $menu_links;
}
