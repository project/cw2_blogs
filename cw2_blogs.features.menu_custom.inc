<?php

/**
 * Implementation of hook_menu_default_menu_custom().
 */
function cw2_blogs_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-control-panel
  $menus['menu-control-panel'] = array(
    'menu_name' => 'menu-control-panel',
    'title' => 'Control Panel',
    'description' => 'This menu generates the links for the Control Panel page so that appropriate permissions can be applied for different levels of users/editors/coordinators. It should be organized into the page sections.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Control Panel');
  t('This menu generates the links for the Control Panel page so that appropriate permissions can be applied for different levels of users/editors/coordinators. It should be organized into the page sections.');


  return $menus;
}
