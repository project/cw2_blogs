<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function cw2_blogs_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function cw2_blogs_node_info() {
  $items = array(
    'blog_post' => array(
      'name' => t('Blog post'),
      'module' => 'features',
      'description' => t('Create a new blog post for your local neighborhood blog reel.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => t('Please remember to select your neighborhood from the list at the bottom of the post.'),
    ),
  );
  return $items;
}
